export class Pacientes {
  public id: number;
  public login: string;
  public firstName: string;
  public lastName: string;
  public email: string;
  public imageUrl: string;
  public activated: boolean;
  public langKey: string;
  public createdBy: string;
  public createdDate: string;
  public lastModifiedBy: string;
  public lastModifiedDate: string;
  public authorities: string;
  public userId: number;
  public cpf: number;
  public celular: number;
  public dataNascimento: string;
  public genero: string;
  public plano: string;
  public pagamentoPaciente: string;
  public atendimentos: string[];
}
