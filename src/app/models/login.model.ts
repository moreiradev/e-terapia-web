export class Login {
  public password: string;
  public rememberMe: boolean;
  public username: string;
}
export class Token {
  public idToken: string;
}
