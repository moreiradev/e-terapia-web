export function logout() {
  const tokenLogout = localStorage.getItem('token');
  if (tokenLogout !== null) {
    localStorage.removeItem('token');
    localStorage.clear();
  }
}
