import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarPsicologoComponent } from './adicionar-psicologo.component';

describe('AdicionarPsicologoComponent', () => {
  let component: AdicionarPsicologoComponent;
  let fixture: ComponentFixture<AdicionarPsicologoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarPsicologoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarPsicologoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
