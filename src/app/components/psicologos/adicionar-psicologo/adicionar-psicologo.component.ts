import { Component, OnInit } from '@angular/core';
import { PsicologosService } from 'src/app/services/psicologos/psicologos.service';
import { Psicologos } from 'src/app/models/psicologos.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-adicionar-psicologo',
  templateUrl: './adicionar-psicologo.component.html',
  styleUrls: ['./adicionar-psicologo.component.css']
})
export class AdicionarPsicologoComponent implements OnInit {
  psicologo = new Psicologos();
  constructor(
    private route: Router,
    private servicePsicologos: PsicologosService,
    private spinner: NgxSpinnerService
  ) {}
  ngOnInit() {
    // tslint:disable-next-line: only-arrow-functions
    $(document).ready(function() {
      $('ul.tabs li').click(function() {
        // tslint:disable-next-line: variable-name
        const tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $('#' + tab_id).addClass('current');
      });
    });
  }
  criarPsi(): void {
    this.spinner.show();
    const psicologo = this.psicologo;
    this.psicologo.activated = true;
    this.psicologo.createdBy = '';
    this.psicologo.createdDate = '';
    this.psicologo.lastModifiedBy = '';
    this.psicologo.lastModifiedDate = '';
    this.psicologo.imageUrl = '';
    this.psicologo.langKey = '';
    this.servicePsicologos.createPsicologo(psicologo).subscribe(
      (data: Psicologos) => {
        this.spinner.hide();
        this.route.navigate(['/logada/psicologos']);
      },
      error => {
        this.spinner.hide();
        console.error(error);
      }
    );
  }
}
