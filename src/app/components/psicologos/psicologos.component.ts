import { Component, OnInit } from '@angular/core';
import { PsicologosService } from 'src/app/services/psicologos/psicologos.service';
import { Psicologos } from 'src/app/models/psicologos.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { ModalService } from 'src/app/services/modal.service';
@Component({
  selector: 'app-psicologos',
  templateUrl: './psicologos.component.html',
  styleUrls: ['./psicologos.component.css']
})
export class PsicologosComponent implements OnInit {
  psicologosList: Psicologos[] = [];
  termSearch: any;
  constructor(
    private modalService: ModalService,
    private servicePsicologos: PsicologosService,
    private spinner: NgxSpinnerService,
    private route: Router
  ) {
    this.getAllPsi();
  }
  ngOnInit() {}
  getAllPsi() {
    this.servicePsicologos.getAllPsicologos().subscribe(
      (data: any) => {
        this.spinner.hide();
        this.psicologosList = data;
        // console.log('Psicologos', this.psicologosList);
      },
      (error: any) => {
        this.spinner.hide();
        console.error(error);
      }
    );
  }
  selectedPsicologo(id: number) {
    this.spinner.show();
    this.servicePsicologos.getOnePsicologo(id).subscribe(
      data => {
        if (data !== undefined) {
          this.route.navigate(['/logada/alterar-psicologo']);
          this.spinner.hide();
          // console.log('Unique psi', data);
          this.servicePsicologos.actualPsiAtt(data, true);
        }
      },
      error => {
        console.error(error);
        this.spinner.hide();
      }
    );
  }
  novoPsicologo() {
    this.route.navigate(['/logada/adicionar-psicologo']);
  }
  openModal(): void {
    this.modalService.open();
  }
}
