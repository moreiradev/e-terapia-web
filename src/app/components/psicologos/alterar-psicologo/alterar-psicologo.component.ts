import { Component, OnInit } from '@angular/core';
import { PsicologosService } from 'src/app/services/psicologos/psicologos.service';
import { Psicologos } from 'src/app/models/psicologos.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-alterar-psicologo',
  templateUrl: './alterar-psicologo.component.html',
  styleUrls: ['./alterar-psicologo.component.css']
})
export class AlterarPsicologoComponent implements OnInit {
  psicologo = new Psicologos();
  constructor(
    private route: Router,
    private servicePsicologos: PsicologosService,
    private spinner: NgxSpinnerService
  ) {
    this.getOnePsi();
  }

  ngOnInit() {
    // tslint:disable-next-line: only-arrow-functions
    $(document).ready(function() {
      $('ul.tabs li').click(function() {
        // tslint:disable-next-line: variable-name
        const tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $('#' + tab_id).addClass('current');
      });
    });
  }
  getOnePsi() {
    this.spinner.show();
    this.servicePsicologos.currentPsi.subscribe(
      (data: any) => {
        if (data !== undefined) {
          this.psicologo = data;
          console.log('Atual psi', this.psicologo);
          this.spinner.hide();
        }
      },
      error => {
        this.spinner.hide();
        console.error(error);
      }
    );
  }
  alterarPsi() {
    this.spinner.show();
    const body = this.psicologo;
    this.servicePsicologos.alterarPsicologo(body).subscribe(
      data => {
        this.spinner.hide();
        console.log(data);
        this.route.navigate(['/logada/psicologos']);
      },
      error => {
        this.spinner.hide();
        console.error(error);
      }
    );
  }
}
