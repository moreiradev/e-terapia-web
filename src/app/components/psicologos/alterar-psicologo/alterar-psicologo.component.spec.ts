import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlterarPsicologoComponent } from './alterar-psicologo.component';

describe('AlterarPsicologoComponent', () => {
  let component: AlterarPsicologoComponent;
  let fixture: ComponentFixture<AlterarPsicologoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlterarPsicologoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlterarPsicologoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
