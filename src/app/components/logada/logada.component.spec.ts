import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogadaComponent } from './logada.component';

describe('LogadaComponent', () => {
  let component: LogadaComponent;
  let fixture: ComponentFixture<LogadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
