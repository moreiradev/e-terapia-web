import { Component, OnInit } from '@angular/core';
import { logout } from './../../utils/generalUtils';
declare var $: any;
@Component({
  selector: 'app-logada',
  templateUrl: './logada.component.html',
  styleUrls: ['./logada.component.css']
})
export class LogadaComponent implements OnInit {
  constructor() {}
  ngOnInit() {
    // tslint:disable-next-line: only-arrow-functions
    $('#menu-toggle').on('click', function() {
      $('#sidebar-wrapper').toggleClass('toggled');
      $('#main').toggleClass('active-main');
    });
  }
  logout(): void {
    logout();
  }
}
