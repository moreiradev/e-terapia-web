import { Component, OnInit } from '@angular/core';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import { Pacientes } from 'src/app/models/pacientes.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.css']
})
export class PacientesComponent implements OnInit {
  pacientesList: Pacientes[] = [];
  termSearch: any;
  constructor(
    private route: Router,
    private spinner: NgxSpinnerService,
    private servicePaciente: PacientesService
  ) {
    this.getAllPaciente();
  }

  ngOnInit() {}

  getAllPaciente() {
    this.servicePaciente.getAllPaciente().subscribe(
      (data: any) => {
        this.pacientesList = data;
        // console.log('allPacientes', data);
      },
      error => {
        console.error(error);
      }
    );
  }
  selectedPaciente(id: number) {
    this.spinner.show();
    this.servicePaciente.getOnePaciente(id).subscribe(
      data => {
        if (data !== undefined) {
          this.route.navigate(['/logada/exibir-paciente']);
          this.spinner.hide();
          this.servicePaciente.actualPacAtt(data, true);
        }
      },
      error => {
        console.error(error);
        this.spinner.hide();
      }
    );
  }
}
