import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExibirPacienteComponent } from './exibir-paciente.component';

describe('ExibirPacienteComponent', () => {
  let component: ExibirPacienteComponent;
  let fixture: ComponentFixture<ExibirPacienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExibirPacienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExibirPacienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
