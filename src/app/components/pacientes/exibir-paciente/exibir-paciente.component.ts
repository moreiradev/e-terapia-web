import { Component, OnInit } from '@angular/core';
import { Pacientes } from 'src/app/models/pacientes.model';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;
@Component({
  selector: 'app-exibir-paciente',
  templateUrl: './exibir-paciente.component.html',
  styleUrls: ['./exibir-paciente.component.css']
})
export class ExibirPacienteComponent implements OnInit {
  paciente = new Pacientes();
  constructor(
    private spinner: NgxSpinnerService,
    private route: Router,
    private servicePaciente: PacientesService
  ) {
    this.getOnePaciente();
  }

  ngOnInit() {
    // tslint:disable-next-line: only-arrow-functions
    $(document).ready(function() {
      $('ul.tabs li').click(function() {
        // tslint:disable-next-line: variable-name
        const tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $('#' + tab_id).addClass('current');
      });
    });
  }
  getOnePaciente() {
    this.spinner.show();
    this.servicePaciente.currentPaciente.subscribe(
      (data: any) => {
        this.spinner.hide();
        console.log(data);
        this.paciente = data;
      },
      error => {
        this.spinner.hide();
        console.error(error);
      }
    );
  }
  putPaciente() {
    this.spinner.show();
    const body = this.paciente;
    this.servicePaciente.putPaciente(body).subscribe(
      (data: any) => {
        this.spinner.hide();
        this.route.navigate(['/logada/pacientes']);
      },
      error => {
        this.spinner.hide();
        console.error(error);
      }
    );
  }
}
