import { TokenService } from './../../shared/token.service';
import { LoginService } from '../../services/login/login.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Login } from 'src/app/models/login.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login = new Login();
  error: boolean;
  constructor(
    private token: TokenService,
    private http: HttpClient,
    private loginService: LoginService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
    this.error = false;
  }

  ngOnInit() {}
  loginUser() {
    this.spinner.show();
    this.login.rememberMe = true;
    this.loginService.login(this.login).subscribe(
      (data: any) => {
        this.error = false;
        this.spinner.hide();
        // console.log(data);
        this.router.navigate(['logada/psicologos']);
        this.token.attToken(data.id_token);
      },
      error => {
        this.error = true;
        this.spinner.hide();
        console.error(error);
      }
    );
  }
}
