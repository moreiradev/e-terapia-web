import { ExibirPacienteComponent } from './components/pacientes/exibir-paciente/exibir-paciente.component';
import { AdicionarPsicologoComponent } from './components/psicologos/adicionar-psicologo/adicionar-psicologo.component';
import { LogadaComponent } from './components/logada/logada.component';
import { HomeComponent } from './components/home/home.component';
import { PacientesComponent } from './components/pacientes/pacientes.component';
import { RelatoriosComponent } from './components/relatorios/relatorios.component';
import { PsicologosComponent } from './components/psicologos/psicologos.component';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogoutComponent } from './components/logout/logout.component';
import { AlterarPsicologoComponent } from './components/psicologos/alterar-psicologo/alterar-psicologo.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'logada',
    component: LogadaComponent,
    children: [
      {
        path: 'logout',
        component: LogoutComponent
      },
      {
        path: 'psicologos',
        component: PsicologosComponent
      },
      {
        path: 'adicionar-psicologo',
        component: AdicionarPsicologoComponent
      },
      {
        path: 'alterar-psicologo',
        component: AlterarPsicologoComponent
      },
      {
        path: 'exibir-paciente',
        component: ExibirPacienteComponent
      },
      {
        path: 'pacientes',
        component: PacientesComponent
      },
      {
        path: 'relatorios',
        component: RelatoriosComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
