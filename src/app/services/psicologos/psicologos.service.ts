import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Psicologos } from 'src/app/models/psicologos.model';
import { environment } from 'src/environments/environment.prod';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class PsicologosService {
  psicologo: Psicologos;
  public psicologoShared = new BehaviorSubject(this.psicologo);
  currentPsi = this.psicologoShared.asObservable();
  constructor(
    private http: HttpClient,
    private route: Router,
    private spinner: NgxSpinnerService
  ) {}

  public getAllPsicologos(): Observable<Psicologos> {
    return this.http.get<Psicologos>(`${environment.API_URL}/api/psicologos`);
  }
  public getOnePsicologo(id: number): Observable<Psicologos> {
    return this.http.get<Psicologos>(
      `${environment.API_URL}/api/psicologos/${id}`
    );
  }
  public actualPsiAtt(psicologo?: Psicologos, addClick?: boolean) {
    const psiNew = new Psicologos();
    if (addClick === true) {
      this.psicologoShared.next(psicologo);
    } else if (addClick === false && psicologo === undefined) {
      this.psicologoShared.next(psiNew);
    }
  }
  public createPsicologo(body: Psicologos): Observable<Psicologos> {
    return this.http.post<Psicologos>(
      `${environment.API_URL}/api/psicologos`,
      body
    );
  }
  public alterarPsicologo(body: Psicologos): Observable<Psicologos> {
    return this.http.put<Psicologos>(
      `${environment.API_URL}/api/psicologos`,
      body
    );
  }
}
