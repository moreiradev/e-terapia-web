import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment.prod';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Pacientes } from './../../models/pacientes.model';

@Injectable({
  providedIn: 'root'
})
export class PacientesService {
  paciente: Pacientes;
  public pacienteShared = new BehaviorSubject(this.paciente);
  public currentPaciente = this.pacienteShared.asObservable();
  constructor(private http: HttpClient) {}
  getAllPaciente(): Observable<Pacientes> {
    return this.http.get<Pacientes>(`${environment.API_URL}/api/pacientes`);
  }
  public getOnePaciente(id: number): Observable<Pacientes> {
    return this.http.get<Pacientes>(
      `${environment.API_URL}/api/pacientes/${id}`
    );
  }
  public actualPacAtt(pacientes?: Pacientes, addClick?: boolean) {
    const paciente = new Pacientes();
    if (addClick === true) {
      this.pacienteShared.next(pacientes);
    } else if (addClick === false && pacientes === undefined) {
      this.pacienteShared.next(paciente);
    }
  }
  public putPaciente(body: Pacientes): Observable<Pacientes> {
    return this.http.put<Pacientes>(
      `${environment.API_URL}/api/pacientes`,
      body
    );
  }
}
