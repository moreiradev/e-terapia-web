import { environment } from '../../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Login } from '../../models/login.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpClient) {}

  public login(body: Login): Observable<Login> {
    return this.http.post<Login>(
      `${environment.API_URL}/api/authenticate`,
      body
    );
  }
}
