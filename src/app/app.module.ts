import { Interceptor } from './auth/interceptor.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { ModalService } from './services/modal.service';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { PsicologosComponent } from './components/psicologos/psicologos.component';
import { PacientesComponent } from './components/pacientes/pacientes.component';
import { RelatoriosComponent } from './components/relatorios/relatorios.component';
import {
  APP_BASE_HREF,
  LocationStrategy,
  HashLocationStrategy
} from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { LogoutComponent } from './components/logout/logout.component';
import { LogadaComponent } from './components/logada/logada.component';
import { AdicionarPsicologoComponent } from './components/psicologos/adicionar-psicologo/adicionar-psicologo.component';
import { ExibirPacienteComponent } from './components/pacientes/exibir-paciente/exibir-paciente.component';
declare var $: any;
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AlterarPsicologoComponent } from './components/psicologos/alterar-psicologo/alterar-psicologo.component';
import { ModalComponent } from './components/modal/modal.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PsicologosComponent,
    PacientesComponent,
    RelatoriosComponent,
    HomeComponent,
    LogoutComponent,
    LogadaComponent,
    AdicionarPsicologoComponent,
    ExibirPacienteComponent,
    AlterarPsicologoComponent,
    ModalComponent
  ],
  imports: [
    FormsModule,
    Ng2SearchPipeModule,
    Interceptor,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxSpinnerModule
  ],
  providers: [
    ModalService,
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
