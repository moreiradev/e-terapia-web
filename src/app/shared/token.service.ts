import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  token: string;
  public tokenShared = new BehaviorSubject(this.token);
  currentToken = this.tokenShared.asObservable();
  constructor() {}
  attToken(token: string) {
    this.tokenShared.next(token);
    const tokenStorage = localStorage.getItem('token');
    if (tokenStorage === null || tokenStorage === undefined) {
      localStorage.setItem('token', JSON.stringify(token));
    }
  }
}
